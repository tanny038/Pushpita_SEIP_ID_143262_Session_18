<?php

class MagicCons{

    public function __construct($value)
    {
        echo $value. "<br/>";
    }

    public function __unset($name)//unset can be called also when the unset obj not exists.
    {
       echo $name . "<br/>";
        // TODO: Implement __unset() method.
    }

    public function __destruct()
    {
        echo "Its Destroyed ... <br/>";
    }
}

$obj = new MagicCons("Its Meeeeee...  __construct() ");

unset($obj);
echo "Hello!!!!";
