<?php

class MagicCons
{
    //We'll learn the custom serialize.
    public $myProp1;
    public $myProp2;
    public $myProp3;
    public $myProp4;

    public function __construct($value)
    {
        echo $value . "<br/>";
    }


    public function __wakeup()//wakeup activates when unserialize method called.Unserialize called to the elements.
    {
        echo " Inside the wake up . <br/> ";
        return array('myProp1','myProp2');//these elements are set to null.
        // TODO: Implement __sleep() method.
    }
}

$obj = new MagicCons("hi");
$result = serialize($obj);//serialize called & it activates the sleep method .
$arr = unserialize($result);//unserialize called & it activates the wake up method .
//var_dump($result);
echo $result;
