<?php

class MagicCons
{
    //We'll learn the custom serialize.
    public $myProp1;
    public $myProp2;
    public $myProp3;
    public $myProp4;

    public function __construct($value)
    {
        echo $value . "<br/>";
    }

    public function __sleep()//sleep activates when serialize method called.Serialize called to serial/chain the elements.
    {
        echo " Inside the sleep . <br/> ";
        return array('myProp1','myProp2');//these elements are set to null.
        // TODO: Implement __sleep() method.
    }
}

$obj = new MagicCons("hi");
$result = serialize($obj);//serialize called & it activates the sleep method .
//var_dump($result);
echo $result;
