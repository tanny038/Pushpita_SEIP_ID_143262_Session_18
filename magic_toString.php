<?php

class Magic
{

    public $foo;

    public function __construct($foo)//local variable
    {
        $this->foo = $foo;//local variable is assigned by global variable.
    }

    public function __toString()
    {
        return $this->foo;//global variable returned
        //return "world!!";
        //return strval(10000);
    }
}

$class = new Magic('Hello');
echo $class;

?>